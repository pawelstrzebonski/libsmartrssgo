# libsmartrssgo

## About

Backend library for machine learning enhanced RSS feed managers (written in Go). It is used in a few front-end programs, such as the [native Smart-RSS Go app](https://gitlab.com/pawelstrzebonski/smart-rss-go) and the [server/web UI Smart-RSS Go Web app](https://gitlab.com/pawelstrzebonski/smart-rss-go-web).

For more information about program design/implementation/use, please consult the [documentation pages](https://pawelstrzebonski.gitlab.io/libsmartrssgo/).

## Usage/Installation

As of writing this project is not stable and the API liable to change without notice. For usage, see the [native Smart-RSS Go app](https://gitlab.com/pawelstrzebonski/smart-rss-go) and [Smart-RSS Go Web](https://gitlab.com/pawelstrzebonski/smart-rss-go-web)
