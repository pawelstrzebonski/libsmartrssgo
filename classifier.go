package libsmartrssgo

import (
	"github.com/jbrukh/bayesian"
	"log"
	"math"
	"regexp"
	"sort"
	"strings"
)

// Normalize given string by removing all non-Latin (and space) characters
// and converting to lowercase
func textNormalize(s string) string {
	// Convert to lowercase
	s2 := strings.ToLower(s)
	// Take only lowercase (latin alphabet) and space values
	reg, err := regexp.Compile("[^a-z ]+")
	if err != nil {
		log.Fatal(err)
	}
	s2 = reg.ReplaceAllString(s2, "")
	return s2
}

// Define the classifier classes
const (
	Liked     bayesian.Class = "Liked"     // User has liked the item
	Neutral   bayesian.Class = "Neutral"   // User has not given an opinion on the item
	Disliked  bayesian.Class = "Disliked"  // User has disliked the item
	Opened    bayesian.Class = "Opened"    // User has opened the item
	NotOpened bayesian.Class = "NotOpened" // User has not opened the item
)

// Internal type for storing pair of classifiers
type MyClassifier struct {
	ratingClass *bayesian.Classifier // Predict rating of item
	openedClass *bayesian.Classifier // Predict whether item will be opened
	isTfIdf     bool                 // Whether using TF-IDF instead of naive Bayes
}

// Given feed item, return list of associated words to pass to classifiers.
// Optionally remove words not seen in classifier training.
func (c *MyClassifier) itemToInput(item MyItem, filterunseen bool) []string {
	// Do not classify based on words shorter than this # of characters
	MINWORDLEN := 4
	input := make([]string, 0)
	for _, word := range strings.Split(textNormalize(item.Title), " ") {
		if len(word) >= MINWORDLEN {
			input = append(input, word)
		}
	}
	for _, word := range strings.Split(textNormalize(item.Summary), " ") {
		if len(word) >= MINWORDLEN {
			input = append(input, word)
		}
	}
	for _, word := range strings.Split(textNormalize(item.Tag), " ") {
		if len(word) >= MINWORDLEN {
			input = append(input, word)
		}
	}
	if filterunseen {
		inputfiltered := make([]string, 0)
		wordfreqs := c.ratingClass.WordFrequencies(input)
		for i, word := range input {
			if wordfreqs[0][i]+wordfreqs[1][i]+wordfreqs[2][i] > 0 {
				inputfiltered = append(inputfiltered, word)
			} else {
				log.Print("Ignoring unseen word ", word)
			}
		}
		return inputfiltered
	} else {
		return input
	}
}

// Setup and return the classifiers, training them on the provided items
func ClassifiersSetup(items []MyItem, isTfIdf bool) MyClassifier {
	if isTfIdf {
		c1 := bayesian.NewClassifierTfIdf(Liked, Neutral, Disliked)
		c2 := bayesian.NewClassifierTfIdf(Opened, NotOpened)
		c := MyClassifier{c1, c2, isTfIdf}
		c.Update(items)
		c.ratingClass.ConvertTermsFreqToTfIdf()
		c.openedClass.ConvertTermsFreqToTfIdf()
		return c
	} else {
		c1 := bayesian.NewClassifier(Liked, Neutral, Disliked)
		c2 := bayesian.NewClassifier(Opened, NotOpened)
		c := MyClassifier{c1, c2, isTfIdf}
		c.Update(items)
		return c
	}
}

// Update classifiers on the provided list of feed items
func (c *MyClassifier) Update(items []MyItem) {
	for _, item := range items {
		input := c.itemToInput(item, false)
		var output bayesian.Class
		if item.Rating == -1 {
			output = Disliked
		} else if item.Rating == 1 {
			output = Liked
		} else {
			output = Neutral
		}
		c.ratingClass.Learn(input, output)
		if item.Opened {
			output = Opened
		} else {
			output = NotOpened
		}
		c.openedClass.Learn(input, output)
	}
}

// If given number is a NaN, return 0, else the number itself
func nanToZero(num float64) float64 {
	if math.IsNaN(num) || math.IsInf(num, 0) {
		return 0.0
	} else {
		return num
	}
}

// Calculate a score for the given string using the classifiers.
// Either score on probability or relative log-probability scales.
func (c *MyClassifier) stringScoreGet(input []string, logscore bool) float64 {
	if logscore {
		ratprobs, _, _ := c.ratingClass.LogScores(input)
		opprobs, _, _ := c.openedClass.LogScores(input)
		var ratprob float64
		if ratprobs[0] > ratprobs[2] {
			ratprob = ratprobs[0]
		} else {
			ratprob = -ratprobs[2]
		}
		return nanToZero(ratprob) + nanToZero(opprobs[0])
	} else {
		ratprobs, _, _, err := c.ratingClass.SafeProbScores(input)
		if err != nil {
			log.Print("Rating classifier: ", err)
		}
		opprobs, _, _, err := c.openedClass.SafeProbScores(input)
		if err != nil {
			log.Print("Opened classifier: ", err)
		}
		return nanToZero(ratprobs[0]) - nanToZero(ratprobs[2]) + nanToZero(opprobs[0])
	}
}
func (c *MyClassifier) itemScoreGet(item MyItem, logscore bool) float64 {
	return c.stringScoreGet(c.itemToInput(item, false), logscore)
}

// Calculate a score for each given item in a list using the classifiers
func (c *MyClassifier) ScoresGet(items []MyItem) []float64 {
	scores := make([]float64, len(items))
	for i, item := range items {
		scores[i] = c.itemScoreGet(item, true)
	}
	return scores
}

type ScoredWord struct {
	Word  string
	Score float64
}

// Return a list of words and associated scores
func (s *SmartRSSBackend) AllWordScoresGet() []ScoredWord {
	c := s.class
	// Setup map of words to scores
	wordscores := make(map[string]float64)
	// Iterate over each class in classifier
	classes := []bayesian.Class{Liked, Neutral, Disliked}
	for _, class := range classes {
		// Iterate over each word in class
		words := c.ratingClass.WordsByClass(class)
		for word, _ := range words {
			// If the word has not already been scored...
			_, ok := wordscores[word]
			if !ok {
				// Score it and add to map
				wordscores[word] = c.stringScoreGet([]string{word}, true)
			}
		}
	}
	// Repeat for next class
	classes = []bayesian.Class{Opened, NotOpened}
	for _, class := range classes {
		words := c.openedClass.WordsByClass(bayesian.Class(class))
		for word, _ := range words {
			_, ok := wordscores[word]
			if !ok {
				wordscores[word] = c.stringScoreGet([]string{word}, true)
			}
		}
	}
	// Convert from map to array of structs
	n := len(wordscores)
	scoredwords := make([]ScoredWord, n)
	i := 0
	for k, v := range wordscores {
		scoredwords[i] = ScoredWord{k, v}
		i++
	}
	// Sort words by score
	sort.Slice(scoredwords, func(i1, i2 int) bool {
		return scoredwords[i1].Score > scoredwords[i2].Score
	})
	return scoredwords
}
