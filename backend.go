package libsmartrssgo

import (
	"bufio"
	"encoding/gob"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strings"
	"time"
)

// Internal type for RSS feed items
type MyItem struct {
	Url        string  // URL of item
	Title      string  // Title of item
	Summary    string  // Text summary of item
	Tag        string  // Tags associated with feed item
	Rating     int     // User's rating of an item (+1/-1 for liked/disliked, or 0 for no opinion)
	Opened     bool    // Whether user has opened the item
	Seen       bool    // Whether user has been shown this item
	Score      float64 // Classifier assigned score (liable to change at any time)
	LastUpdate uint64  // Time of fetching feed item
}

// Internal type for url (index) to score sorting and iteration
type ItemScores struct {
	url   string
	score float64
}

// Internal type for RSS feed items
type MyFeed struct {
	Url          string // URL of item
	Title        string // Title of item
	LastUpdate   uint64 // Last time fetching the feed
	UpdatePeriod uint64 // Minimal time between fetching feed
}

// "Sanitize" provided string by removing formatting tags and replacing all whitespace with spaces
func textSanitize(s string) string {
	// Replace tabs/newlines with spaces
	s = strings.Replace(s, "\n", " ", -1)
	s = strings.Replace(s, "\t", " ", -1)
	// Get rid of HTML tags
	reg, err := regexp.Compile("<.*?>")
	if err != nil {
		log.Fatal(err)
	}
	s = reg.ReplaceAllString(s, "")
	// Remove all '&X;' codes
	reg, err = regexp.Compile("&.*?;")
	if err != nil {
		log.Fatal(err)
	}
	s = reg.ReplaceAllString(s, "")
	return s
}

// Internal types for hacking together RSS/Atom parsing
type rssFeed struct {
	// Actually for Atom format
	Title string     `xml:"title"` // Title of feed
	Url   rssLink    `xml:"link"`  // URL of feed
	Items []*rssItem `xml:"entry"` // List of items
	// Repeat for RSS
	Title2 string     `xml:"channel>title"` // Title of feed
	Url2   rssLink    `xml:"channel>link"`  // URL of feed
	Items2 []*rssItem `xml:"channel>item"`  // List of items
}
type rssLink struct {
	Url  string `xml:"href,attr"` // URL of item
	Url2 string `xml:",chardata"` // URL of item
}
type rssItem struct {
	Title        string   `xml:"title"`       // Title of item
	Link         rssLink  `xml:"link"`        // URL of item
	Description  string   `xml:"description"` // Item description
	Description2 string   `xml:"summary"`     // Item description
	Categories   []string `xml:"category"`    // List of item categories
}

// Fetch and parse the RSS feed for the given URL
func (s *SmartRSSBackend) feedGet(url string) *rssFeed {
	// Fetch the feed
	resp, err := http.Get(url)
	s.logErr(err)
	defer resp.Body.Close()
	// Read body of respons
	body, err := io.ReadAll(resp.Body)
	s.logErr(err)
	// Parse response as our hacky RSS/Atom feed
	var feed rssFeed
	xml.Unmarshal(body, &feed)
	return &feed
}

// Structure for serializing database save-file
type FileFormat struct {
	Dbfeeds map[string]MyFeed // Feeds data
	Dbitems map[string]MyItem // Feed items data
}

// Setup and return the backend database.
// If the database does not exist, create it with the required tables.
func DBSetup(filename string) (map[string]MyFeed, map[string]MyItem) {
	var dbfeeds map[string]MyFeed
	var dbitems map[string]MyItem
	_, err := os.Stat(filename)
	if err == nil {
		f, err := os.Open(filename)
		if err != nil {
			log.Fatal(err, "Open Error")
		}
		defer f.Close()
		r := bufio.NewReader(f)
		dec := gob.NewDecoder(r)
		var ff FileFormat
		err = dec.Decode(&ff)
		if err != nil {
			log.Fatal(err, "Decode Feeds Error")
		}
		dbfeeds = ff.Dbfeeds
		dbitems = ff.Dbitems
	}
	if errors.Is(err, os.ErrNotExist) {
		dbfeeds = make(map[string]MyFeed)
		dbitems = make(map[string]MyItem)
	}
	return dbfeeds, dbitems
}

// Save database information to a file
func (s *SmartRSSBackend) DBSave() {
	if len(s.filename) == 0 {
		fmt.Println("No-save-file mode, not saving")
		return
	}
	if !s.updated {
		fmt.Println("Databases not changed since last save, not saving")
		return
	}
	fmt.Println("Saving file")
	f, err := os.Create(s.filename)
	s.logErrFatal(err)
	defer f.Close()
	r := bufio.NewWriter(f)
	enc := gob.NewEncoder(r)
	s.m.Lock()
	ff := FileFormat{s.dbfeeds, s.dbitems}
	err = enc.Encode(ff)
	s.logErrFatal(err)
	// Flush to ensure data is written to disk
	err = r.Flush()
	s.logErrFatal(err)
	s.updated = false
	s.m.Unlock()
	fmt.Println("Saved file")
}

// Add a feed to list of RSS to check by URL, and set update period to specified duration (in seconds)
// Will fetch items from feed upon adding
func (s *SmartRSSBackend) FeedAdd(url string, updateperiod uint64) {
	feed := s.feedGet(url)
	if feed != nil {
		newfeed := MyFeed{Url: url, Title: feed.Title + feed.Title2, LastUpdate: uint64(time.Now().Unix()), UpdatePeriod: updateperiod}
		s.m.Lock()
		s.dbfeeds[url] = newfeed
		s.updated = true
		s.m.Unlock()
		s.log(fmt.Sprint("Added feed ", url))
		s.feedUpdate(url)
		// Sort the items
		s.m.Lock()
		sort.Slice(s.itemslist, func(i, j int) bool { return s.itemslist[j].score > s.itemslist[i].score })
		s.m.Unlock()
	}
}

// Add an RSS item (produced by gofeed) to database
func (s *SmartRSSBackend) itemAdd(item *rssItem) {
	url := item.Link.Url
	if len(url) == 0 {
		url = item.Link.Url2
	}
	newitem := MyItem{url, textSanitize(item.Title), textSanitize(item.Description) + textSanitize(item.Description2),
		textSanitize(strings.Join(item.Categories, " ")), 0, false, false, 0.0, uint64(time.Now().Unix())}
	newitem.Score = s.class.itemScoreGet(newitem, true)
	s.m.Lock()
	_, ok := s.dbitems[newitem.Url]
	if !ok {
		s.dbitems[newitem.Url] = newitem
		s.updated = true
		s.itemslist = append(s.itemslist, ItemScores{newitem.Url, newitem.Score})
	}
	s.m.Unlock()
	s.log(fmt.Sprint("Item added"))
}

// Update the RSS feed's items for feed with given URL
func (s *SmartRSSBackend) feedUpdate(url string) {
	feed := s.feedGet(url)
	if feed != nil {
		if s.class.isTfIdf && s.classifierOutdated {
			s.log(fmt.Sprint("Recreating classifier"))
			s.log(fmt.Sprint("# of seen items: ", len(s.ItemsSeenGet())))
			newclass := ClassifiersSetup(s.ItemsSeenGet(), s.class.isTfIdf)
			s.class = &newclass
			s.classifierOutdated = false
			s.log(fmt.Sprint("Recreated classifier"))
		}
		for _, item := range feed.Items {
			s.itemAdd(item)
		}
		for _, item := range feed.Items2 {
			s.itemAdd(item)
		}
		// Set time of latest update
		s.m.Lock()
		f := s.dbfeeds[url]
		f.LastUpdate = uint64(time.Now().Unix())
		s.dbfeeds[url] = f
		s.updated = true
		s.m.Unlock()
	}
}

// Update all RSS feeds that are due for an update
func (s *SmartRSSBackend) FeedsUpdate() {
	s.m.Lock()
	feeds := make([]MyFeed, 0, len(s.dbfeeds))
	now := uint64(time.Now().Unix())
	for _, v := range s.dbfeeds {
		if v.LastUpdate+v.UpdatePeriod < now {
			feeds = append(feeds, v)
		}
	}
	s.m.Unlock()
	for _, feed := range feeds {
		s.log(fmt.Sprint("Updating ", feed.Title))
		s.feedUpdate(feed.Url)
	}
	// If any feeds were updated, then sort the items
	if len(feeds) > 0 {
		s.m.Lock()
		sort.Slice(s.itemslist, func(i, j int) bool { return s.itemslist[j].score > s.itemslist[i].score })
		s.m.Unlock()
	}
}

// Return list of all RSS feeds in database
func (s *SmartRSSBackend) FeedsList() []MyFeed {
	s.m.Lock()
	n := len(s.dbfeeds)
	feeds := make([]MyFeed, n, n)
	i := 0
	for _, v := range s.dbfeeds {
		feeds[i] = v
		i++
	}
	s.m.Unlock()
	return feeds
}

// Return list of all RSS items in database
func (s *SmartRSSBackend) itemsList() []MyItem {
	s.m.Lock()
	n := len(s.dbitems)
	items := make([]MyItem, n, n)
	i := 0
	for _, v := range s.dbitems {
		items[i] = v
		i++
	}
	s.m.Unlock()
	return items
}

// Return list of all RSS items that have not been shown to user.
// Items returned in order of highest score.
func (s *SmartRSSBackend) ItemsUnreadGet() []MyItem {
	s.m.Lock()
	// Get # of times
	n := len(s.itemslist)
	items := make([]MyItem, n, n)
	// Lookup each of those items and return
	for i := 0; i < n; i++ {
		item := s.itemslist[n-i-1]
		items[i] = s.dbitems[item.url]
	}
	s.m.Unlock()
	return items
}

// Return list of up to specified number of RSS items that have not been shown to user.
// Items returned in order of highest score.
func (s *SmartRSSBackend) ItemsUnreadGetLimited(limit uint64) []MyItem {
	n := int(limit)
	if n < 0 {
		n = 0
	}
	s.m.Lock()
	listlen := len(s.itemslist)
	// Determine # of items to return (lesser of limit and # of items)
	if listlen < n {
		n = listlen
	}
	items := make([]MyItem, n, n)
	// Get items from the list
	for i := 0; i < n; i++ {
		item := s.itemslist[listlen-i-1]
		items[i] = s.dbitems[item.url]
	}
	s.m.Unlock()
	return items
}

// Return list of RSS items that have been shown to user
func (s *SmartRSSBackend) ItemsSeenGet() []MyItem {
	s.m.Lock()
	items := make([]MyItem, 0, len(s.dbitems))
	for _, v := range s.dbitems {
		if v.Seen {
			items = append(items, v)
		}
	}
	s.m.Unlock()
	return items
}

// "Like" RSS item with given URL in database
func (s *SmartRSSBackend) ItemLike(url string) {
	s.m.Lock()
	i := s.dbitems[url]
	i.Rating = 1
	s.dbitems[url] = i
	s.updated = true
	s.classifierOutdated = true
	s.m.Unlock()
	s.log(fmt.Sprint("Liked ", url))
}

// "Dislike" RSS item with given URL in database
func (s *SmartRSSBackend) ItemDislike(url string) {
	s.m.Lock()
	i := s.dbitems[url]
	i.Rating = -1
	s.dbitems[url] = i
	s.updated = true
	s.classifierOutdated = true
	s.m.Unlock()
	s.log(fmt.Sprint("Disliked ", url))
}

func remove(slice []ItemScores, s int) []ItemScores {
	return append(slice[:s], slice[s+1:]...)
}

// Mark RSS item with given URL in database as opened by user
func (s *SmartRSSBackend) ItemMarkOpened(url string) {
	s.m.Lock()
	i := s.dbitems[url]
	i.Opened = true
	s.dbitems[url] = i
	s.updated = true
	s.classifierOutdated = true
	s.m.Unlock()
	s.log(fmt.Sprint("Opened ", url))
}

// Mark RSS item with given URL in database as seen by user
func (s *SmartRSSBackend) ItemMarkViewed(url string) {
	s.m.Lock()
	i := s.dbitems[url]
	i.Seen = true
	s.dbitems[url] = i
	s.updated = true
	s.classifierOutdated = true
	n := len(s.itemslist)
	// Iterate over list of unseen items
	for idx := n - 1; idx >= 0; idx-- {
		item := s.itemslist[idx]
		// If it is in the list, remove it
		if item.url == url {
			s.itemslist = remove(s.itemslist, idx)
			break
		}
	}
	s.m.Unlock()
	items := make([]MyItem, 1)
	items[0] = i
	if !s.class.isTfIdf {
		s.class.Update(items)
	}
	s.log(fmt.Sprint("Viewed ", url))
}

// Set the score in database for the RSS item with given URL
func (s *SmartRSSBackend) ItemSetScore(url string, score float64) {
	s.m.Lock()
	i := s.dbitems[url]
	i.Score = score
	s.dbitems[url] = i
	s.updated = true
	s.m.Unlock()
	s.log(fmt.Sprint("Updated score on ", url))
}

// Remove RSS feed with given URL from the database
func (s *SmartRSSBackend) FeedRemove(url string) bool {
	s.m.Lock()
	delete(s.dbfeeds, url)
	s.updated = true
	s.m.Unlock()
	s.log(fmt.Sprint("Removing feed ", url))
	return true
}
