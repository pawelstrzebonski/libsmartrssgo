# Dependencies

This application makes use of the following Go packages:

* [`bayesian`](https://github.com/jbrukh/bayesian): Implements classification/rating of feed links based on their textual features
