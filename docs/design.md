# Design Document

## Files

This application is broken into three primary files:

* `backend.go`
* `classifier.go`
* `main.go`

The `backend.go` file handles the database that manages the RSS feeds and the items that those feed have served to the user. RSS feeds are fetched and parsed using DIY RSS/Atom parsing based on standard library XML parser.

The `classifier.go` file implements the text classification functionality which uses `bayesian` package for predicting whether or not a user will open a link from an RSS feed, and whether the will "like" or "dislike" it, where (dis)liking a link is a local in-app process. These probabilities are used to "score" and RSS item.

The `main.go` file ties together the backend and classifier functionality and exposes it as a `SmartRSSBackend` type.

## Backend

RSS feeds are fetched and parsed using DIY RSS/Atom parsing based on standard library XML parser. This uses `rssFeed`, `rssLink`, and `rssItem` as intermediate structs for the XML parser to extract the key data from RSS/atom feeds, to be later converted into internal structs for feeds and items.

A pair of hashmaps act as a database that stores `MyItem` structures and `MyFeed` structures as well as the writing/reading of said structures.

The `MyFeed` structure stores the following information for a given RSS feed:

* "Url": The URL of the RSS feed
* "Title": The title of the RSS feed
* "LastUpdate": The date/time of the last time the feed was updated from its source
* "UpdatePeriod": The minimal duration (in seconds) between updates of this source

This data is largely static. Rows are are inserted/deleted when the user adds/removes an RSS feed, and whenever a feed is updated the date/time of update is modified.

The `MyItem` structure stores the main information of the feed items obtained from the feed sources. In particular, for each item served by an RSS source it stores:

* "Url": The URL of the item
* "Title": The title of the item
* "Summary": The summary text of the item
* "Tags": A string of the feed-provided tags pertinent to the item
* "Viewed": A boolean indicating whether or not this item has been shown to the user in this application
* "Seen": A boolean indicating whether or not the user has opened this item via a front-end application
* "Rating": An integer indicating whether the user has "liked" this item (+1), "disliked" it (-1), or neither (0)
* "Score": A float of the classifier's latest rating of this feed item
* "LastUpdate": The date/time of the item being added to the database

A additional list of URL-score pairs (`ItemScores` structures) is used for quicker, ordered look-up of unread item URLs, which can then be used to look-up the feed item data in the primary hashmap "database".

## Classifier

The `bayesian` package is used to estimate the probability that will either click on a link or (dis)like within this application. As of writing, it uses a Naive Bayes classifier that is trained on links in the backend's database that are marked as been already shown to the user (and have been either "liked" or "disliked" for the predictor of "rating"). These predictions are based on a combination of the textual content of the items's `Title`, `Summary` and `Tags`.
