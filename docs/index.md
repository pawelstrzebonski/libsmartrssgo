# libsmartrssgo

libsmartrssgo is a backend library for making machine learning enhanced RSS feed managers, written in Go. It is a spin-off of my prior [Smart-RSS Go](https://gitlab.com/pawelstrzebonski/smart-rss-go) project and code, created with the intention of providing a common backend core that can be used for different frontend applications.

Existing front-ends include [Smart-RSS Go](https://gitlab.com/pawelstrzebonski/smart-rss-go) and [Smart-RSS Go Web](https://gitlab.com/pawelstrzebonski/smart-rss-go-web).
