package libsmartrssgo

import (
	"fmt"
	"log"
	"sort"
	"sync"
	"time"
)

// Logging level (verbosity)
type LogLevel int

// Define constants for the logging verbosity level
const (
	ALL LogLevel = iota
	ERROR
	FATAL
)

// Back-end structure containing all database and classifier objects
type SmartRSSBackend struct {
	dbfeeds            map[string]MyFeed // database of feeds
	dbitems            map[string]MyItem // database of feed items
	itemslist          []ItemScores      // List for sorting feed items by score
	class              *MyClassifier     // Text classifier
	logger             *LogLevel         // Back-end logging verbosity level
	m                  sync.Mutex        // Mutex gating database access
	filename           string            // Filename of database save-file
	updated            bool              // Whether databases have been updated after last save
	classifierOutdated bool              // Whether the classifier dataset is potentially outdated due to new items being seen
}

// Setup back-end with database at given file location and
// set the logging verbosity to given level
func Setup(filename string, ll LogLevel, isTfIdf bool) *SmartRSSBackend {
	dbfeeds, dbitems := DBSetup(filename)
	s := SmartRSSBackend{dbfeeds: dbfeeds, dbitems: dbitems, filename: filename, updated: false, classifierOutdated: true}
	// Setup list of new items using unseen items
	itemslist := make([]ItemScores, 0)
	for _, v := range s.dbitems {
		if !v.Seen {
			itemslist = append(itemslist, ItemScores{url: v.Url, score: v.Score})
		}
	}
	s.itemslist = itemslist
	sort.Slice(s.itemslist, func(i, j int) bool { return s.itemslist[j].score > s.itemslist[i].score })
	// Setup classifier using previously seen items
	class := ClassifiersSetup(s.ItemsSeenGet(), isTfIdf)
	s.class = &class
	s.logger = &ll
	var m sync.Mutex
	s.m = m
	return &s
}

// Log given string if ALL logging is enabled
func (s *SmartRSSBackend) log(str string) {
	if *s.logger == ALL {
		log.Print(str)
	}
}

// Check if given object is an error. If so, log it unless
// only FATAL error logging enabled.
func (s *SmartRSSBackend) logErr(err error) {
	if err != nil && *s.logger != FATAL {
		log.Print("Error: ", err)
	}
}

// Check if given object is an error. If so, log it unconditionally.
func (s *SmartRSSBackend) logErrFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// Set automatic RSS feed update checking to run at given duration (in seconds)
func (s *SmartRSSBackend) SetAutoUpdate(updateperiod int) {
	// Setup a regular feed update ticker
	go func() {
		for now := range time.Tick(time.Second * time.Duration(updateperiod)) {
			s.log(fmt.Sprint(now, " => Regular Update Check"))
			s.FeedsUpdate()
		}
	}()
}

// Set automatic database saving run at given duration (in seconds)
func (s *SmartRSSBackend) SetAutoSave(updateperiod int) {
	// Setup a regular feed update ticker
	go func() {
		for now := range time.Tick(time.Second * time.Duration(updateperiod)) {
			s.log(fmt.Sprint(now, " => Regular Database Saving"))
			s.DBSave()
		}
	}()
}

// Save back-end components.
func (s *SmartRSSBackend) Close() {
	s.DBSave()
}
