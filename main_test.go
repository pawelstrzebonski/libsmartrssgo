package libsmartrssgo

import "testing"
import "fmt"

func TestEmptyBayes(t *testing.T) {
	// Initial setup of back-end with in-memory database
	lsrg := Setup("", FATAL, false)

	// Test no feeds to start with
	got := len(lsrg.FeedsList())
	want := 0
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Test no items to start with
	got = len(lsrg.ItemsUnreadGet())
	want = 0
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}
}
func TestEmptyTFIDF(t *testing.T) {
	// Initial setup of back-end with in-memory database
	lsrg := Setup("", FATAL, true)

	// Test no feeds to start with
	got := len(lsrg.FeedsList())
	want := 0
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Test no items to start with
	got = len(lsrg.ItemsUnreadGet())
	want = 0
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}
}

// Benchmark time to setup back-end
func BenchmarkSetupBayes(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Setup("", FATAL, false)
	}
}
func BenchmarkSetupTFIDF(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Setup("", FATAL, true)
	}
}

func TestSingleFeedBayes(t *testing.T) {
	// Initial setup of back-end with in-memory database
	lsrg := Setup("", FATAL, false)

	// Add a feed
	feedurl := "https://blog.golang.org/feed.atom?format=xml"
	lsrg.FeedAdd(feedurl, 600)

	// Update feeds (should do nothing right now)
	lsrg.FeedsUpdate()

	// Test single feeds
	got := len(lsrg.FeedsList())
	want := 1
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Test some items
	items0 := lsrg.ItemsUnreadGet()
	got = len(items0)
	want = 0
	if got <= want {
		t.Errorf("got %d, wanted > %d", got, want)
	}

	// All of the items should be unread
	items := lsrg.itemsList()
	got = len(items)
	want = len(items0)
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Mark an item seen
	lsrg.ItemMarkViewed(items0[0].Url)

	// Expect to see 1 less unseen item
	items = lsrg.ItemsUnreadGet()
	want = len(items0) - 1
	got = len(items)
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Expect to see 1 less than all items
	want = len(items) - 1
	items = lsrg.ItemsUnreadGetLimited(uint64(want))
	got = len(items)
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Expect to see 1 seen item
	items = lsrg.ItemsSeenGet()
	want = 1
	got = len(items)
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Mark next items as opened/scored
	items = lsrg.ItemsUnreadGet()
	for i, v := range items {
		if i%2 == 0 {
			lsrg.ItemLike(v.Url)
			lsrg.ItemMarkOpened(v.Url)
		} else {
			lsrg.ItemDislike(v.Url)
		}
		lsrg.ItemMarkViewed(v.Url)
	}

	// Remove only feed
	lsrg.FeedRemove(feedurl)
	got = len(lsrg.FeedsList())
	want = 0
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Add another feed and score the items
	feedurl = "https://golangbyexample.com/feed/"
	lsrg.FeedAdd(feedurl, 600)

	// Get items (should be scored now with non-zero scores)
	items = lsrg.ItemsUnreadGet()
	if items[0].Score < items[len(items)-1].Score {
		t.Errorf("item scores should be in descending order")
	}

	// These should not do much in testing
	lsrg.SetAutoUpdate(60 * 60)
	lsrg.SetAutoSave(60 * 60)

	// Get a list of word scores
	ws := lsrg.AllWordScoresGet()
	if ws[0].Score < ws[len(ws)-1].Score {
		t.Errorf("Word scores should be in descending order")
	}

	// This should do nothing now
	lsrg.DBSave()
	lsrg.Close()
}
func TestSingleFeedTFIDF(t *testing.T) {
	// Initial setup of back-end with in-memory database
	lsrg := Setup("", FATAL, true)
	fmt.Println("Done setup")

	// Add a feed
	feedurl := "https://blog.golang.org/feed.atom?format=xml"
	lsrg.FeedAdd(feedurl, 600)
	fmt.Println("Done add feed")

	// Update feeds (should do nothing right now)
	lsrg.FeedsUpdate()
	fmt.Println("Done feeds update")

	// Test single feeds
	got := len(lsrg.FeedsList())
	want := 1
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}
	fmt.Println("Done feeds list")

	// Test some items
	items0 := lsrg.ItemsUnreadGet()
	got = len(items0)
	want = 0
	if got <= want {
		t.Errorf("got %d, wanted > %d", got, want)
	}

	// All of the items should be unread
	items := lsrg.itemsList()
	got = len(items)
	want = len(items0)
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Mark an item seen
	lsrg.ItemMarkViewed(items0[0].Url)

	// Expect to see 1 less unseen item
	items = lsrg.ItemsUnreadGet()
	want = len(items0) - 1
	got = len(items)
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Expect to see 1 less than all items
	want = len(items) - 1
	items = lsrg.ItemsUnreadGetLimited(uint64(want))
	got = len(items)
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Expect to see 1 seen item
	items = lsrg.ItemsSeenGet()
	want = 1
	got = len(items)
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Mark next items as opened/scored
	items = lsrg.ItemsUnreadGet()
	for i, v := range items {
		if i%2 == 0 {
			lsrg.ItemLike(v.Url)
			lsrg.ItemMarkOpened(v.Url)
		} else {
			lsrg.ItemDislike(v.Url)
		}
		lsrg.ItemMarkViewed(v.Url)
	}

	// Remove only feed
	lsrg.FeedRemove(feedurl)
	got = len(lsrg.FeedsList())
	want = 0
	if got != want {
		t.Errorf("got %d, wanted %d", got, want)
	}

	// Add another feed and score the items
	feedurl = "https://golangbyexample.com/feed/"
	lsrg.FeedAdd(feedurl, 600)

	// Get items (should be scored now with non-zero scores)
	items = lsrg.ItemsUnreadGet()
	if items[0].Score < items[len(items)-1].Score {
		t.Errorf("item scores should be in descending order")
	}

	// These should not do much in testing
	lsrg.SetAutoUpdate(60 * 60)
	lsrg.SetAutoSave(60 * 60)

	// Get a list of word scores
	ws := lsrg.AllWordScoresGet()
	if ws[0].Score <= ws[len(ws)-1].Score {
		t.Errorf("Word scores should be in descending order")
	}

	// This should do nothing now
	lsrg.DBSave()
	lsrg.Close()
}

func TestSingleFeedFileBayes(t *testing.T) {
	// Setup a testing in-file database
	lsrg := Setup("test.gob", FATAL, false)

	// Add a feed (may or may not exist already)
	feedurl := "https://blog.golang.org/feed.atom?format=xml"
	lsrg.FeedAdd(feedurl, 600)

	// This may or may not do something
	lsrg.DBSave()
	lsrg.Close()
}
